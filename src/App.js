import React, { Component } from 'react';
import './App.css';
import Keyboard from "./component/Keyboard/Keyboard";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Keyboard/>
      </div>
    );
  }
}

export default App;
