const initialState = {
    password:'2266',
    currentPassword: '',
    stars: '',
    isCorrect: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'ADD':
            if (state.currentPassword.length >= 4) return state;
            return {
                ...state,
                currentPassword: state.currentPassword + action.value,
                stars: state.stars + "*",
            };

        case 'REMOVE':
            const newPassword = state.stars.substr(0, state.stars.length - 1);
            const newStars = state.currentPassword.substr(0, state.currentPassword.length - 1);
            return {
                ...state,
                currentPassword: newStars,
                isCorrect: false,
                stars: newPassword
            };

        case 'ENTER':
            if (state.password === state.currentPassword) {
                return {
                    ...state,
                    isCorrect: true
                }
            }
    }
    return state;
    };


export default reducer;

