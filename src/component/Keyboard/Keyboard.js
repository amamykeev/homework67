import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";

import './Keyboard.css';

class Keyboard extends Component {
    render() {
        return (
            <Fragment>
                <div className='background'>
                <div className="Keyboard">
                    <div className={this.props.isCorrect ? 'PasswordWindowGreen': 'PasswordWindowRed'}>{this.props.stars}</div>
                    <div className='Buttons'>
                        <ul>
                            <li><button onClick={this.props.numberClick}>7</button></li>
                            <li><button onClick={this.props.numberClick}>4</button></li>
                            <li><button onClick={this.props.numberClick}>1</button></li>
                            <li><button onClick={this.props.removeNumber}>x</button></li>
                        </ul>
                        <ul>
                            <li><button onClick={this.props.numberClick}>8</button></li>
                            <li><button onClick={this.props.numberClick}>5</button></li>
                            <li><button onClick={this.props.numberClick}>2</button></li>
                            <li><button onClick={this.props.numberClick}>0</button></li>
                        </ul>
                        <ul>
                            <li><button onClick={this.props.numberClick}>9</button></li>
                            <li><button onClick={this.props.numberClick}>6</button></li>
                            <li><button onClick={this.props.numberClick}>3</button></li>
                            <li><button onClick={this.props.enter}>enter</button></li>
                        </ul>
                    </div>
                </div>
                </div>
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        stars: state.stars,
        isCorrect: state.isCorrect
    }
};

const mapDispatchToProps = dispatch => {
    return {
        numberClick: (e) => dispatch({type: 'ADD', value: e.target.innerText}),
        removeNumber: () => dispatch({type: 'REMOVE'}),
        enter: () => dispatch({type: 'ENTER'})
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Keyboard);